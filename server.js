let votes = {};
let counter = [];
let clientId = 1;
const http = require("http");
const express = require("express");
const app = express();

app.use(express.static("public"));

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/public/index.html");
});

const port = process.env.PORT || 3000;

const server = http.createServer(app).listen(port, function () {
  console.log("Listening on port " + port + ".");
});

const WebSocket = require("ws");

const wss = new WebSocket.Server({ port: 4000, clientTracking: true });

function countVotes(votes) {
  var voteCount = {
    A: 0,
    B: 0,
    C: 0,
    D: 0,
  };

  for (let vote in votes) {
    voteCount[votes[vote]]++;
  }
  return voteCount;
}

wss.on("open", function open() {
  console.log("connection");
  wss.send(Date.now());
});

wss.on("connection", (ws) => {
  ws.id = clientId++;
  counter.push(ws.id);

  console.log("A user has connected.", wss.clients.size);

  // argument should be array, string
  ws.send(JSON.stringify({ status: "You are connected" }));

  // broadcast message
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify({ countUser: wss.clients.size }));
    }
  });

  ws.on("message", (e) => {
    let data = JSON.parse(e);
    if ("voteCast" in data) {
      votes[ws.id] = data.voteCast;
      ws.send(JSON.stringify({ personalVote: data.voteCast }));

      wss.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify({ voteCount: countVotes(votes) }));
        }
      });
    }
    console.log("received: %s", e);
  });

  ws.on("close", function () {
    console.log("A user has disconnected.", wss.clients.size);
    delete votes[ws.id];
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({ voteCount: countVotes(votes) }));
        client.send(JSON.stringify({ countUser: wss.clients.size }));
      }
    });
  });
});

module.exports = server;
