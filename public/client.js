const connectionCount = document.getElementById("connection-count");
const statusMessage = document.getElementById("status-message");
const buttons = document.querySelectorAll("#choices button");
const aTotal = document.querySelector(".a-total");
const bTotal = document.querySelector(".b-total");
const cTotal = document.querySelector(".c-total");
const dTotal = document.querySelector(".d-total");
const personalVote = document.querySelector(".vote");
const voteContainer = document.getElementById("personal-vote");

let ws = new WebSocket("ws://localhost:4000");
// event emmited when connected
ws.onopen = function () {
  console.log("websocket is connected ...");

  // sending a send event to websocket server
  ws.send(JSON.stringify({ message: "Ask to connect" }));
};

// event emmited when receiving message
ws.onmessage = function (e) {
  let data = JSON.parse(e.data);

  if ("status" in data) {
    statusMessage.innerHTML = data.status;
  }

  if ("countUser" in data) {
    connectionCount.innerText = "Connected Users: " + data.countUser;
  }

  if ("voteCount" in data) {
    aTotal.innerText = "Total A Votes:" + " " + data.voteCount.A;
    bTotal.innerText = "Total B Votes:" + " " + data.voteCount.B;
    cTotal.innerText = "Total C Votes:" + " " + data.voteCount.C;
    dTotal.innerText = "Total D Votes:" + " " + data.voteCount.D;
  }

  if ("personalVote" in data) {
    personalVote.innerText = data.personalVote;
  }
};

for (let i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener("click", function () {
    ws.send(JSON.stringify({ voteCast: this.innerText }));
  });
}

function showVote() {
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function () {
      voteContainer.style.display = "block";
    });
  }
}

showVote();
